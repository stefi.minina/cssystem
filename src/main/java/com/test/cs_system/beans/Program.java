package com.test.cs_system.beans;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Program {
    @Id
    private int id;
   
    private int student; //id
    private int subject; //id
    private int active;

    public Program(){
        active = 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudent() {
        return student;
    }

    public void setStudent(int student) {
        this.student = student;
    }


    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }


}
