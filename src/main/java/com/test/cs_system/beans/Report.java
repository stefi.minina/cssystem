package com.test.cs_system.beans;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Report {

    @Id
    private int id;
    private String report;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }
}
