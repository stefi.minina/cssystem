package com.test.cs_system.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;



@Entity
public class Subject {
    @Id
    private int id;
    @Size(min = 6, message = "{name.cannot.be.less.then.six.digits}")
    private String name;
    private int credits;

    private int fk; //User.id

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int course) {
        this.credits = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFk() {
        return fk;
    }

    public void setFk(int fk) {
        this.fk = fk;
    }


}
