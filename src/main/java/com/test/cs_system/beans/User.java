package com.test.cs_system.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Size;

@Entity
public class User {

    @Id
    private int id;

    @DecimalMax(value = "5", message = "{course.max.value.is.five}")
    private int course;

    @Size(min = 6, message = "{name.cannot.be.less.then.six.digits}")
    private String name;
    private String degree;

    public User(){}
    public User(int id, String name, int course){
        setId(id);
        setName(name);
        setCourse(course);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }
}
