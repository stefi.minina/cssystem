package com.test.cs_system.beans.dtos;

import com.test.cs_system.beans.User;

public class FullDataDto extends User{
    private int id;
    private int number;
    private String nameOfSubject;

    public FullDataDto(){}
    public FullDataDto(int id, String name, String nameOfSubject, int number){
        super.setName(name);
        this.nameOfSubject = nameOfSubject;
        this.setNumber(number);
        
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getNameOfSubject(){
        return nameOfSubject;
    }
    public void setNameOfSubject(String nameOfSubject){
        this.nameOfSubject = nameOfSubject;
    }

}
