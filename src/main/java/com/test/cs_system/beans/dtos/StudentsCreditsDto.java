package com.test.cs_system.beans.dtos;

import com.test.cs_system.beans.Subject;

public class StudentsCreditsDto extends Subject{
    private int id;
    private String nameOfStudent;

    public StudentsCreditsDto(){}
    public StudentsCreditsDto(int id, String nameOfStudent, int credits){
        this.nameOfStudent = nameOfStudent;
        super.setCredits(credits);
        
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getNameOfStudent(){
        return nameOfStudent;
    }

}
