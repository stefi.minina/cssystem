package com.test.cs_system.beans.dtos;

public class StudentsSubjectsDto{
    private int id;
    private String nameOfStudent;
    private String nameOfSubject;

    public StudentsSubjectsDto(){}
    public StudentsSubjectsDto(int id, String nameOfStudent, String nameOfSubject ){
        this.nameOfStudent = nameOfStudent;
        this.nameOfSubject = nameOfSubject;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getNameOfStudent(){
        return nameOfStudent;
    }
    public String getNameOfSubject(){
        return nameOfSubject;
    }
}
