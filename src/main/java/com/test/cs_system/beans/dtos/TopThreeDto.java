package com.test.cs_system.beans.dtos;
import  com.test.cs_system.beans.Subject;

public class TopThreeDto extends Subject{
    private int count;

    public TopThreeDto(){}
    public TopThreeDto(int count, String name){
        super.setName(name);
        this.count = count;
    }

    public int getCount(){
        return count;
    }

    public void setCount(int count){
        this.count = count;
    }
}
