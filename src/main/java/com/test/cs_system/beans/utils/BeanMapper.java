package com.test.cs_system.beans.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import com.test.cs_system.beans.dtos.*;
import com.test.cs_system.beans.User;

public class BeanMapper {
    private static BeanMapper beanMapper;

    private BeanMapper() {
    }

    public static BeanMapper getInstance() {
        if (beanMapper == null)
            beanMapper = new BeanMapper();
        return beanMapper;
    }

    public List<StudentsSubjectsDto> toStudentsSubjectsDtos(ResultSet resultSet) throws SQLException{
        List<StudentsSubjectsDto> list = new ArrayList<>();
        int i = 1;
        resultSet.beforeFirst();
        while (resultSet.next()){
        list.add(new StudentsSubjectsDto(i, resultSet.getString("student"),
                    resultSet.getString("subject")));
             System.out.println("student " + resultSet.getString("student") + "subject"+  resultSet.getString("subject"));
             i++;
        }
        System.out.println(list.size());
        return list;
    }

    public List<StudentsCreditsDto> toStudentsCreditsDtos(ResultSet resultSet) throws SQLException{
        List<StudentsCreditsDto> list = new ArrayList<>();
        int i = 1;
        resultSet.beforeFirst();
        while (resultSet.next()){
        list.add(new StudentsCreditsDto(i, resultSet.getString("student"), resultSet.getInt("credits")));
             System.out.println("student " + resultSet.getString("student") + " credits "+  resultSet.getInt("credits"));
             i++;
        }
        System.out.println(list.size());
        return list;
    }

    public List<FullDataDto> toFullDataDtos(ResultSet resultSet) throws SQLException{
        List<FullDataDto> list = new ArrayList<>();
        int i = 1;
        resultSet.beforeFirst();
        while (resultSet.next()){
        list.add(new FullDataDto(i, resultSet.getString("name"), resultSet.getString("subject"), resultSet.getInt("number")));
             System.out.println("name " + resultSet.getString("name") + " subject "+  resultSet.getString("subject") + " number Subkjects: " + resultSet.getInt("number") );
             i++;
        }
        System.out.println(list.size());
        return list;
    }

    public List<TopThreeDto> toTopThreeDtos(ResultSet resultSet) throws SQLException{
        List<TopThreeDto> list = new ArrayList<>();
        resultSet.last();
        System.out.println(resultSet.getRow());
        resultSet.beforeFirst();
        while (resultSet.next()){
        list.add(new TopThreeDto(resultSet.getInt("count"), resultSet.getString("name")));
             System.out.println("name" + resultSet.getString("name") + " count students: " + resultSet.getInt("count") );
        }
        System.out.println(list.size());
        return list;
    }
    public List<User> toUser(ResultSet resultSet) throws SQLException{
        List<User> list = new ArrayList<>();
        resultSet.beforeFirst();
        while (resultSet.next()){
        list.add(new User(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("course")));
             System.out.println("name" + resultSet.getString("name") + " course students: " + resultSet.getInt("course") );
        }
        System.out.println(list.size());
        return list;
    }
    
    
    
}
