package com.test.cs_system.controllers;

import com.test.cs_system.beans.Program;
import com.test.cs_system.repository.EnrollRepository;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class EnrollController {

    @Autowired 
    private EnrollRepository enrollRepository;

    @PostMapping("/enrollSubject")
    public String enrollSubject(@Valid @ModelAttribute("newEnroll") Program program, BindingResult result, Model model) {
        System.out.println("In registration controller");
        if (result.hasErrors()) {
            return "enroll";
        }

        enrollRepository.save(program);
        model.addAttribute("dataSaved", "User registered successfully");
        return "index";
    }

    @PostMapping("/disEnrollSubject")
    public String disEnrollSubject(@Valid @ModelAttribute("disEnroll") Program program, BindingResult result, Model model) {
        System.out.println("In registration controller");
        if (result.hasErrors()) {
            return "disenroll";
        }    
        Program saveThisProgram = checkIfExist(program);

        if(saveThisProgram == null ){
            model.addAttribute("dataSaved", "No records for filled student and subject");
            return "index";
        }
        else{
            saveThisProgram.setActive(0);
            System.out.println("id" + saveThisProgram.getId() + "student" + saveThisProgram.getStudent() + "active" + saveThisProgram.getActive());
            enrollRepository.save(saveThisProgram);
        }

        model.addAttribute("dataSaved", "User registered successfully");
        return "index";
    }

    private Program checkIfExist(Program inProgram){
        return enrollRepository.searchByEnrollment(inProgram.getStudent(), inProgram.getSubject(), 1);
    }
}
