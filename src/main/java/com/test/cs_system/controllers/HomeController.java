package com.test.cs_system.controllers;

import com.test.cs_system.beans.*;
import com.test.cs_system.beans.dtos.StudentsSubjectsDto;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

	@GetMapping("/home")
	public String goHome() {
		System.out.println("in home controller");
		return "index"; // name of the view that supposed to go
	}

	@GetMapping("/goToSearch")
	public String goToSearch() {
		System.out.println("going to search page");
		return "search";
	}

	@GetMapping("/goToRegister")
	public String goToRegister() {
		System.out.println("going to register page");
		return "register";
	}

	@GetMapping("/goToRegisterSubject")
	public String goToRegisterSubject() {
		System.out.println("going to register page");
		return "registerSubject";
	}

	@GetMapping("/goToEnroll")
	public String goToEnroll() {
		System.out.println("going to enroll page");
		return "enroll";
	}

	@GetMapping("/goToDisEnroll")
	public String goToDisEnroll() {
		System.out.println("going to enroll page");
		return "disenroll";
	}

	@GetMapping("/goToReport")
	public String goToReport() {
		System.out.println("going to report page");
		return "report";
	}

	@ModelAttribute("newuser")
	public User getDefaultUser() {
		return new User();
	}

	@ModelAttribute("newsubject")
	public Subject getDefaultSubject() {
		return new Subject();
	}

	@ModelAttribute("newEnroll")
	public Program getDefaultProgram() {
		return new Program();
	}
	
	@ModelAttribute("disEnroll")
	public Program getDefaultProgramForDisentollment() {
		return new Program();
	}

	@ModelAttribute("takereport")
	public List<StudentsSubjectsDto> getDefaultReport() {
		System.out.println("getDefaultReport");
		return new ArrayList<>();
	}

	@ModelAttribute("degreeItems")
	public List<String> getDefaultItems() {
		return Arrays.asList(new String[] { "Student", "Professor", "Assistant" });
	}


}
