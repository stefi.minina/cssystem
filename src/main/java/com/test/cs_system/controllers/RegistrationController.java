package com.test.cs_system.controllers;

import com.test.cs_system.beans.User;
import com.test.cs_system.beans.Subject;
import com.test.cs_system.repository.UserRepository;
import com.test.cs_system.repository.SubjectRepository;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class RegistrationController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SubjectRepository subjectRepository;

    @PostMapping("/registeruser")
    public String registerUser(@Valid @ModelAttribute("newuser") User user, BindingResult result, Model model) {
        System.out.println("In registration controller");
        if (result.hasErrors()) {
            return "register";
        }
        userRepository.save(user);
        model.addAttribute("dataSaved", "User registered successfully");
        return "index";
    }

    @PostMapping("/registerSubject")
    public String registerSubject(@Valid @ModelAttribute("newsubject") Subject subject, BindingResult result, Model model) {
        System.out.println("In registration controller");
        if (result.hasErrors()) {
            return "registerSubject";
        }
        subjectRepository.save(subject);
        model.addAttribute("dataSaved", "Subject registered successfully");
        return "index";
    }

}
