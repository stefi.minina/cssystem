package com.test.cs_system.controllers;

import com.test.cs_system.beans.User;
import com.test.cs_system.beans.dtos.*;
import com.test.cs_system.repository.ReportRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ch.qos.logback.core.joran.conditional.ElseAction;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ReportController {

    @Autowired
    private ReportRepository reportRepository;

    @GetMapping("/report")
    public String getReport(@RequestParam("report") String report, Model model) {
        System.out.println("In report controller, wanted report: " + report);
        if (report.equalsIgnoreCase("getsubjects")) { 
            System.out.println("getsubjects: Search for data");
            List<StudentsSubjectsDto> studentssubjectsdtos= new ArrayList<>(); 
            studentssubjectsdtos = reportRepository.getStudentsAndSubjects();
            model.addAttribute("studentssubjectsdtos", studentssubjectsdtos);
        }

        else if (report.equalsIgnoreCase("getcredits")) { 
            System.out.println("getcredits: Search for data");
            List<StudentsCreditsDto> studentscreditsdtos= new ArrayList<>(); 
            studentscreditsdtos = reportRepository.getStudentsAndCredits();
            model.addAttribute("studentscreditsdtos", studentscreditsdtos);
        }

        else if (report.equalsIgnoreCase("fullreport")) { 
            System.out.println("fullreport: Search for data");
            List<FullDataDto> fulldatadtos= new ArrayList<>(); 
            fulldatadtos = reportRepository.getFullData();
            model.addAttribute("fulldatadtos", fulldatadtos);
        }
        else if (report.equalsIgnoreCase("topsubject")) { 
            System.out.println("topsubject: Search for data");
            List<TopThreeDto> topthreesubjectsdtos= new ArrayList<>(); 
            topthreesubjectsdtos = reportRepository.getTopThree(1);
            model.addAttribute("topthreesubjectsdtos", topthreesubjectsdtos);
        }
        else if (report.equalsIgnoreCase("topLecturer")) { 
            System.out.println("topLecturer: Search for data");
            List<TopThreeDto> topthreelecturersdtos= new ArrayList<>(); 
            topthreelecturersdtos = reportRepository.getTopThree(2);
            model.addAttribute("topthreelecturersdtos", topthreelecturersdtos);
        }
        else if (report.equalsIgnoreCase("allUsers")) { 
            System.out.println("allUsers: Search for data");
            List<TopThreeDto> allLecturers= new ArrayList<>(); 
            allLecturers = reportRepository.getTopThree(3);
            System.out.println(allLecturers.size());
            model.addAttribute("allLecturers", allLecturers);
            List<User> users = new ArrayList<>(); 
            users = reportRepository.getAllStudents();
            model.addAttribute("users", users);
        }
        else //sth went wrong
            return "index";
        return "report";
    }

        /*
         * if (report.equalsIgnoreCase("subject")) { List<UsersAndSubjectsDto> subjects
         * = new ArrayList<>(); subjects = reportRepository.searchSubject();
         * model.addAttribute("subjects", subjects); }
         */

        /*
         * else if (wantedReport.equalsIgnoreCase("credits")) { List<NamesAndCountsDto>
         * subjects = new ArrayList<>(); subjects = reportRepository.searchCredits();
         * model.addAttribute("subjects", subjects); }
         * 
         * else if (wantedReport.equalsIgnoreCase("number")) { List<FullReportDto>
         * subjects = new ArrayList<>(); subjects = reportRepository.searchFullList();
         * model.addAttribute("subjects", subjects); }
         * 
         * else if (wantedReport.equalsIgnoreCase("topSubject")) {
         * List<NamesAndCountsDto> subjects = new ArrayList<>(); subjects =
         * reportRepository.searchTopThreeSubject(); model.addAttribute("subjects",
         * subjects); }
         * 
         * if (report.equalsIgnoreCase("topLecturer")) {
         * List<LECTURERS_AND_NUMBER_SUBJECTS_VIEW> subjects = new ArrayList<>();
         * subjects = reportRepository.searchTopThreeLecturers();
         * model.addAttribute("subjects", subjects); }
         */

        /*if (report.equals("all")) {
            List<LECTURERSANDNUMBERSUBJECTSVIEW> subjects = new ArrayList<>();
            subjects = reportRepository.searchAllLecturersAndNumberSubjects();
            model.addAttribute("subjects", subjects); // model.addAttribute("subjects", subjects);*/
            // List<LECTURERS_AND_NUMBER_SUBJECTS_VIEW> subjects = new ArrayList<>();
            // subjects = reportRepository.searchAllStudents();
            // model.addAttribute("subjects",subjects); // model.addAttribute("subjects",
            // subjects); }
        
}

