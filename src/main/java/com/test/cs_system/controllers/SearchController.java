package com.test.cs_system.controllers;

import com.test.cs_system.beans.User;
import com.test.cs_system.repository.SelectUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class SearchController {

    @Autowired
    private SelectUserRepository selectUserRepository;

    @GetMapping("/search")
    public String search(@RequestParam("search") String search, Model model){
        System.out.println("in search controller");
        System.out.println("search criteria: "+search);

        List<User> users = new ArrayList<>();
        users = selectUserRepository.searchByName(search);
        model.addAttribute("users", users);
        return "search";
    }

}
