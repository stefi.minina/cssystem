package com.test.cs_system.repository;

import com.test.cs_system.beans.User;
import com.test.cs_system.beans.Program;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnrollRepository extends CrudRepository<Program, Integer> {

    @Query("select s from Program s where s.student = :student and s.subject = :subject and s.active = :active")
    public Program searchByEnrollment(@Param("student") int student, @Param("subject") int subject, @Param("active") int active);

}
