package com.test.cs_system.repository;

import com.test.cs_system.beans.utils.*;
import com.test.cs_system.beans.dtos.*;
import com.test.cs_system.beans.User;

import org.springframework.stereotype.Repository;
import com.test.cs_system.repository.utils.ConnectionWrapper;
import com.test.cs_system.repository.utils.Queries;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class ReportRepository{

    ResultSet resultSet;

    @Autowired
    private ConnectionWrapper connectionWrapper;

    public List<StudentsSubjectsDto> getStudentsAndSubjects(){

        try (Statement statement = connectionWrapper.getConnection().createStatement(resultSet.TYPE_SCROLL_INSENSITIVE, resultSet.FETCH_FORWARD))
        {
            resultSet = statement.executeQuery(Queries.STUDENTS_ENROLLED_SUBJECTS);
            return  resultSet.first() ? BeanMapper.getInstance().toStudentsSubjectsDtos(resultSet) : null;
           
        }catch(SQLException exc){
            System.out.println("Sqlexc " + exc);
        }
        return null;
    }

    public List<StudentsCreditsDto> getStudentsAndCredits(){

        try (Statement statement = connectionWrapper.getConnection().createStatement(resultSet.TYPE_SCROLL_INSENSITIVE, resultSet.FETCH_FORWARD))
        {
            resultSet = statement.executeQuery(Queries.STUDENTS_AND_CREDITS);
            return  resultSet.first() ? BeanMapper.getInstance().toStudentsCreditsDtos(resultSet) : null;
           
        }catch(SQLException exc){
            System.out.println("Sqlexc " + exc);
        }
        return null;
    }

    
    public List<FullDataDto> getFullData(){

        try (Statement statement = connectionWrapper.getConnection().createStatement(resultSet.TYPE_SCROLL_INSENSITIVE, resultSet.FETCH_FORWARD))
        {
            resultSet = statement.executeQuery(Queries.LECTURERS_SUBJECTS_AND_NUMBER_ENROLLED_STUDENTS);
            return  resultSet.first() ? BeanMapper.getInstance().toFullDataDtos(resultSet) : null;
           
        }catch(SQLException exc){
            System.out.println("Sqlexc " + exc);
        }
        return null;
    }
    
    public List<TopThreeDto> getTopThree(int mode){

        try (Statement statement = connectionWrapper.getConnection().createStatement(resultSet.TYPE_SCROLL_INSENSITIVE, resultSet.FETCH_FORWARD))
        {
            switch(mode){
                case 1:
                    resultSet = statement.executeQuery(Queries.TOP_THREE_SUBJECT);
                    break;
                case 2:
                    resultSet = statement.executeQuery(Queries.TOP_THREE_LECTURERS);
                    break;
                case 3:
                    resultSet = statement.executeQuery(Queries.ALL_LECTURERS);
                    break;
                default:
                    return null;
            }
            return  resultSet.first() ? BeanMapper.getInstance().toTopThreeDtos(resultSet) : null;
           
        }catch(SQLException exc){
            System.out.println("Sqlexc " + exc);
        }
        return null;
    }

    public List<User> getAllStudents(){
        try (Statement statement = connectionWrapper.getConnection().createStatement(resultSet.TYPE_SCROLL_INSENSITIVE, resultSet.FETCH_FORWARD))
        {
            resultSet = statement.executeQuery(Queries.ALL_STUDENTS);
            return  resultSet.first() ? BeanMapper.getInstance().toUser(resultSet) : null;
           
        }catch(SQLException exc){
            System.out.println("Sqlexc " + exc);
        }
        return null; 
    }
    
    

    public void searchAllLecturersAndNumberSubjects(){
       
        try (Statement statement = connectionWrapper.getConnection().createStatement(resultSet.TYPE_SCROLL_INSENSITIVE, resultSet.FETCH_FORWARD))
        {
            resultSet = statement.executeQuery("SELECT User.name nameUser, COUNT(Subject.id) count FROM User LEFT JOIN Subject ON User.id = Subject.fk_id_user WHERE User.degree<>'Student' GROUP BY User.id ORDER BY nameUser ASC;");
            resultSet = statement.executeQuery("SELECT * FROM User;");
            //int iNbRows = resultSet.getRow();
		    
           // int affRows = statement.executeUpdate("DROP  TABLE IF EXISTS `Subjects`; CREATE TABLE `Subject` (id int NOT NULL AUTO_INCREMENT, name VARCHAR(100) NOT NULL, credits int NOT NULL, FK_id_user int NOT NULL, PRIMARY KEY (`id`),FOREIGN KEY (`FK_id_user`)  REFERENCES `User`(`id`)) ENGINE=MyISAM DEFAULT CHARSET=cp1251;");
            //int affRows = statement.executeUpdate("DROP  TABLE IF EXISTS `Subjects`;");
            //System.out.println("nb Aff rows " + affRows);
            /*
            try {
                //resultSet.beforeFirst();
                ResultSetMetaData rsMd = resultSet.getMetaData();
                int iColumnCount = rsMd.getColumnCount();

                //CHECK COLUMN NAMES
                /*String[] columns = new String[iColumnCount];

                try{
                    for(int i = 0; i< iColumnCount; i++){
                        columns[i] = rsMd.getColumnLabel(i+1);
                        System.out.println(columns[i]);
                }
                }catch(SQLException exc){
                    System.out.println("Sqlexc " + exc);
                }
                
                if(iNbRows == 0) {
                    System.out.println("no rows");
                    return;
                }
                String[] strRows = new String[iNbRows];
                int j = 0;
                while (resultSet.next()) {
                    for (int i = 1; i <= iColumnCount; i++) {
                        if(strRows[j] == null)
                            strRows[j] = resultSet.getString(i);
                        else 
                            strRows[j] += resultSet.getString(i);
                    }
                    System.out.println(strRows);
                    j++;
                }
                
                //return resultSet.first() ? ModelMapper.getInstance().toDiscipline(resultSet) : null;
            } catch (SQLException e) {
                System.out.println("SQL exception occured" + e);
            }
            
        }catch(SQLException exc){
            System.out.println("Sqlexc " + exc);
        }

        
    
		

        //return ModelMapper.getInstance().toTeachersWithDisciplinesCount(resultSet);
*/
    }catch(SQLException exc){
        System.out.println("Sqlexc " + exc);
    }
}

}

