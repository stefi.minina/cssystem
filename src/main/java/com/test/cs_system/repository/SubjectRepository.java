package com.test.cs_system.repository;

import com.test.cs_system.beans.User;
import com.test.cs_system.beans.Subject;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, Integer> {

    @Query("select s from User s where s.name like %:name%")
    public List<User> searchByNameSubj(@Param("name") String name);
}
