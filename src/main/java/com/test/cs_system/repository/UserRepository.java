package com.test.cs_system.repository;

import com.test.cs_system.beans.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    @Query("select s from User s where s.name like %:name%")
    public List<User> searchByName(@Param("name") String name);
}
