package com.test.cs_system.repository.utils;

public class Queries {
    public static final String STUDENTS_ENROLLED_SUBJECTS = "SELECT  User.name student, Subject.name subject FROM Program INNER JOIN Subject ON Program.subject = Subject.id INNER JOIN User ON Program.student = User.id WHERE Program.active = 1 AND User.degree = 'Student' ORDER BY student ASC;";
    public static final String STUDENTS_AND_CREDITS = "SELECT User.name student, IFNULL(SUM(Subject.credits),0) credits FROM User LEFT JOIN Program ON User.id=Program.student LEFT JOIN Subject ON Program.subject = Subject.id WHERE User.degree = 'Student' GROUP BY User.id ORDER BY credits,student ASC;";
    public static final String LECTURERS_SUBJECTS_AND_NUMBER_ENROLLED_STUDENTS = "SELECT User.name, Subject.name subject, COUNT(Program.student) number FROM Subject INNER JOIN  User ON Subject.fk = User.id INNER JOIN  Program  ON  Subject.id = Program.subject WHERE Program.active=1 AND User.degree<>'Student' GROUP BY Program.subject ORDER BY Program.subject ASC;";
    public static final String TOP_THREE_SUBJECT = "SELECT Subject.name name, COUNT(Program.subject) count FROM Program RIGHT JOIN Subject  ON  Program.subject = Subject.id GROUP BY Program.subject ORDER BY count DESC LIMIT 3;";
    public static final String TOP_THREE_LECTURERS = "SELECT User.name, COUNT(DISTINCT(Program.student)) count FROM Subject INNER JOIN  User  ON Subject.fk = User.id INNER JOIN  Program ON  Subject.id = Program.subject WHERE User.degree<>'Student' GROUP BY Subject.fk ORDER BY count DESC LIMIT 3;";
    public static final String ALL_LECTURERS = "SELECT User.name, COUNT(Subject.id) count FROM User LEFT JOIN Subject ON User.id = Subject.fk WHERE User.degree<>'Student' GROUP BY User.id ORDER BY User.name ASC;";
    public static final String ALL_STUDENTS = "SELECT User.id, User.name, course FROM User WHERE degree='Student' ORDER BY name ASC;";
}
