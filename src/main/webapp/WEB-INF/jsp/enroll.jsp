<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta charset="UTF-8">
<title>Enroll</title>
	<link rel="stylesheet" href="/css/style.css"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

	<header id="home" class="header">
		<nav class="nav" role="navigation">
			<div class="container nav-elements">
				<div class="branding">
					<a href="#home"><img src="images/logo.jpg"
						alt="Logo - Minina"></a>
				</div>
				<!-- branding -->
				<ul class="navbar">
					<li><a href="/home">Home</a></li>
					<li><a href="/goToSearch">Search</a></li>
					<li><a href="/goToRegister">Register</a></li>
					<li><a href="/goToReport">Report</a></li>

				</ul>
				<!-- navbar -->
			</div>
			<!-- container nav-elements -->
		</nav>
		
		<!-- <div class="container tagline">
    <h1 class="headline">Our Mission</h1>
    <p>We support and encourage <em>active and healthy</em> lifestyles, by offering <em>ethically sourced</em> and <em>eco-friendly</em> nutritional products for the <em>performance-driven</em> athlete.</p>
  </div>container tagline -->
	</header>
	<!-- #home -->
	<span class="success">${dataSaved}</span>
	<section id="registration" class="section">
	 <div class="container tagline">
	 <em>Enroll Student</em><br/>
    		 <form:form method="post" action="/enrollSubject" modelAttribute="newEnroll">
				<label>Student</label> <form:input path="student" type="int" value="0"/><form:errors path="student" cssClass="error"/><br/>
				<label>Subject</label> <form:input path="subject" type="int" value="0"/><form:errors path="subject" cssClass="error"/><br/>
    			<input type="submit" value="Submit" id="submit">
    		</form:form>
		</div>
	</section>
	<footer class="footer">
		<div class="container">
			<nav class="nav" role="navigation">
				<div class="container nav-elements">
					<div class="branding">
						<a href="#home"><img src="images/logo.jpg"
							alt="Logo - Minina"></a>
						<p class="address">1000 Sofia<br>
								Bulgaria
						</p>
					
					</div>
				</div>
			</nav>
			<p class="legal">This page has been created for test purposes</p>
		</div>
		<!-- container -->
	</footer>
	<!-- footer -->




</body>
</html>