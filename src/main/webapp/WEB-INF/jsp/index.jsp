<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>ComputerScienceSystem</title>
  <link rel="stylesheet" href="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<header id="home" class="header">
    <nav class="nav" role="navigation">
        <div class="container nav-elements">
            <div class="branding">
                <a href="#home"><img src="images/logo.jpg" alt="Minina"></a>
            </div><!-- branding -->
            <ul class="navbar">
                <li><a href="/home">Home</a></li>
                <li><a href="/goToSearch">Search</a></li>
                <li><a href="/goToRegister">Registration</a></li>
                <li><a href="/goToRegisterSubject">RegistrationSubjects</a></li>
                <li><a href="/goToEnroll">Enroll</a></li>
                <li><a href="/goToDisEnroll">DisEnroll</a></li>
                <li><a href="/goToReport">Report</a></li>
            </ul><!-- navbar -->
        </div><!-- container nav-elements -->
    </nav>
    <div class="container tagline">
        <h1 class="headline">Our Mission</h1>
        <p><em>Computer Science System</em></p>
    </div><!-- container tagline -->
</header><!-- #home -->
<span class="success">${dataSaved}</span>
<section id="history" class="section">
    <div class="container">
        <div class="text-content">
            <h2 class="headline">University History</h2>
            <p>University traces its origins to a small school that opened in 1838 in Randolph County, North Carolina. Originally a preparatory school for young men called the Union Institute Academy, it was then chartered as a teaching college named Normal College by the state of North Carolina in 1851.</p>
        </div>
    </div><!-- container text -->
</section><!-- #history -->

<footer class="footer">
    <div class="container">
        <nav class="nav" role="navigation">
            <div class="container nav-elements">
                <div class="branding">
                    <a href="#home"><img src="images/logo.jpg" alt="Minina"></a>
                    <p class="address">1000 Sofia<br>
                        Bulgaria
                    </p>
                </div>
            </div>
        </nav>
        <p class="legal">This page has been created for test purposes</p>
    </div><!-- container -->
</footer><!-- footer -->

</body>
</html>
