<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta charset="UTF-8">
<title>Report</title>
	<link rel="stylesheet" href="/css/style.css"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

	<header id="home" class="header">
		<nav class="nav" role="navigation">
			<div class="container nav-elements">
				<div class="branding">
					<a href="#home"><img src="images/logo.jpg"
						alt="Minina"></a>
				</div>
				<!-- branding -->
				<ul class="navbar">
					<li><a href="/home">Home</a></li>
					<li><a href="/goToSearch">Search</a></li>
					<li><a href="/goToRegister">Register</a></li>
					<li><a href="/goToReport">Report</a></li>

				</ul>
				<!-- navbar -->
			</div>
			<!-- container nav-elements -->
		</nav>
		
		<!-- <div class="container tagline">
    <h1 class="headline">Our Mission</h1>
    <p>We support and encourage <em>active and healthy</em> lifestyles, by offering <em>ethically sourced</em> and <em>eco-friendly</em> nutritional products for the <em>performance-driven</em> athlete.</p>
  </div>container tagline -->
	</header>
	<!-- #home -->

	<section id="report" class="section">
		<div class="container tagline">
		<em>Select Report</em><br/>
				<form method="get" action="/report">
				<table>
					<tr><td><input type ="radio" name="report" id="report" value="getsubjects"/>Students with subjects?</td></tr>
					<tr><td><input type ="radio" name="report" id="report" value="getcredits"/>Students and credits that they have?</td></tr>
					<tr><td><input type ="radio" name="report" id="report" value="fullreport"/>List of lecturers, subjects and number of enrolled students?</td></tr>
					<tr><td><input type ="radio" name="report" id="report" value="topsubject"/>Top 3 of most wanted subjects?</td></tr>
					<tr><td><input type ="radio" name="report" id="report" value="topLecturer"/>Busiest lecturers?</td></tr>
					<tr><td><input type ="radio" name="report" id="report" value="allUsers"/>All studenst and lecturers?</td></tr>
				</table><br>
				<input type="submit" value="Submit" id="submit">
			   </form>
		   </div>
	   </section>
	<!-- guarantee -->
	<c:if test="${!empty(studentssubjectsdtos)}">
		<section id="studentssubjectsdtos" class="section">
			<table>
				<tr>
					<th>Student</th>
					<th>Subject</th>
				</tr>
			<c:forEach var="studentssubjectsdto" items="${studentssubjectsdtos}">
					<tr>
						<td><input type="text" name="studentssubjectsdto" value="${studentssubjectsdto.nameOfStudent}"/><br /></td>
						<td><input type="text" name="studentssubjectsdto" value="${studentssubjectsdto.nameOfSubject}"/><br /></td>
					</tr>
			</c:forEach>
			</table>
		</section>
	</c:if>
	<!-- guarantee -->
	<c:if test="${!empty(studentscreditsdtos)}">
		<section id="studentscreditsdtos" class="section">
			<table>
				<tr>
					<th>Student</th>
					<th>Credits</th>
				</tr>
			<c:forEach var="studentscreditsdto" items="${studentscreditsdtos}">
					<tr>
						<td><input type="text" name="studentscreditsdto" value="${studentscreditsdto.nameOfStudent}"/><br /></td>
						<td><input type="text" name="studentscreditsdto" value="${studentscreditsdto.credits}"/><br /></td>
					</tr>
			</c:forEach>
			</table>
		</section>
	</c:if>
	<!-- guarantee -->
	<c:if test="${!empty(fulldatadtos)}">
		<section id="fulldatadtos" class="section">
			<table>
				<tr>
					<th>Lecturer</th>
					<th>Subject</th>
					<th>NumberEnrolledStudentsForSubject</th>
				</tr>
			<c:forEach var="fulldatadto" items="${fulldatadtos}">
					<tr>
						<td><input type="text" name="fulldatadto" value="${fulldatadto.name}"/><br /></td>
						<td><input type="text" name="fulldatadto" value="${fulldatadto.nameOfSubject}"/><br /></td>
						<td><input type="text" name="fulldatadto" value="${fulldatadto.number}"/><br /></td>
					</tr>
			</c:forEach>
			</table>
		</section>
	</c:if>
	<!-- guarantee -->
	<c:if test="${!empty(topthreesubjectsdtos)}">
		<section id="topthreesubjectsdtos" class="section">
			<table>
				<tr>
					<th>Subject</th>
					<th>NumberEnrolledStudents</th>
				</tr>
			<c:forEach var="topthreedto" items="${topthreesubjectsdtos}">
					<tr>
						<td><input type="text" name="topthreedto" value="${topthreedto.name}"/><br /></td>
						<td><input type="text" name="topthreedto" value="${topthreedto.count}"/><br /></td>
					</tr>
			</c:forEach>
			</table>
		</section>
	</c:if>
	<!-- guarantee -->
	<c:if test="${!empty(topthreelecturersdtos)}">
		<section id="topthreelecturersdtos" class="section">
			<table>
				<tr>
					<th>Busiest Lecturers</th>
					<th>NumberEnrolledStudents</th>
				</tr>
			<c:forEach var="topthreedto" items="${topthreelecturersdtos}">
					<tr>
						<td><input type="text" name="topthreedto" value="${topthreedto.name}"/><br /></td>
						<td><input type="text" name="topthreedto" value="${topthreedto.count}"/><br /></td>
					</tr>
			</c:forEach>
			</table>
		</section>
	</c:if>
	<!-- guarantee -->
	<c:if test="${!empty(allLecturers)}">
		<section id="allLecturers" class="section">
			<table>
				<tr>
					<th>Lecturer</th>
					<th>NumberSubjectsThatHave</th>
				</tr>
			<c:forEach var="topthreedto" items="${allLecturers}">
					<tr>
						<td><input type="text" name="topthreedto" value="${topthreedto.name}"/><br /></td>
						<td><input type="text" name="topthreedto" value="${topthreedto.count}"/><br /></td>
					</tr>
			</c:forEach>
			</table>
		</section>
	</c:if>
	<!-- guarantee -->
	<c:if test="${!empty(users)}">
		
		<section id="users" class="section">
			<table>
				<tr>
					<th>Student_id</th>
					<th>Student</th>
					<th>Course</th>
				</tr>
			<c:forEach var="user" items="${users}">
					<tr>
						<td><input type="text" name="user" value="${user.id}"/><br /></td>
						<td><input type="text" name="user" value="${user.name}"/><br /></td>
						<td><input type="text" name="user" value="${user.course}"/><br /></td>
					</tr>
			</c:forEach>
			</table>
		</section>
	</c:if>
	<!-- guarantee -->
	<footer class="footer">
		<div class="container">
			<nav class="nav" role="navigation">
				<div class="container nav-elements">
					<div class="branding">
						<a href="#home"><img src="images/logo.jpg"
							alt="Minina"></a>
						<p class="address">1000 Sofia<br>
								Bulgaria
						</p>
					</div>
				</div>
			</nav>
			<p class="legal">This page has been created for test purposes</p>
		</div>
		<!-- container -->
	</footer>
	<!-- footer -->




</body>
</html>