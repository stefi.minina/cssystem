<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Search</title>
<link rel="stylesheet" href="css/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

	<header id="home" class="header">
		<nav class="nav" role="navigation">
			<div class="container nav-elements">
				<div class="branding">
					<a href="home"><img src="images/logo.jpg"
						alt="Logo - Minina"></a>
				</div>
				<!-- branding -->
				<ul class="navbar">
                                <li><a href="/home">home</a></li>
                                <li><a href="/goToRegister">Register</a></li>
                                <li><a href="/goToSearch">Search</a></li>
                            </ul><!-- navbar -->
				<!-- navbar -->
			</div>
			<!-- container nav-elements -->
		</nav>
	</header>
	<!-- #home -->

	<section id="search" class="section">
		<header class="imageheader"></header>
		<div class="container">
			<h2 class="headline">Search User</h2>
			<form action="/search" method="get">
				<label class="card-title">Search your user</label>
				 <input path="search" name="search" value="">
			    <input type="submit" value="Search">
			</form>
		</div>
	</section>
	<!-- guarantee -->
        <c:if test="${!empty(users)}">
    		<section id="users" class="section">
            <c:forEach var="user" items="${users}">
    		<div class="userContainer">
            				<div class="userContainerItem">
            					<input type="text" name="user"
            						value="${user.name}"><br />

            				</div>
                        
						</c:forEach>
            	</div>
			
            </section>
        </c:if>

	<footer class="footer">
		<div class="container">
			<nav class="nav" role="navigation">
				<div class="container nav-elements">
					<div class="branding">
						<a href="#home"><img src="images/logo.jpg"
							alt="Logo - H Plus Sports"></a>
						<p class="address">
							1000 Sofia
						</p>
					</div>
				</div>
			</nav>
			<p class="legal">This page was created for test purposes</p>
		</div>
		<!-- container -->
	</footer>
	<!-- footer -->




</body>
</html>